﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService : IEmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public async Task<List<Employee>> GetEmployees()
        {
            if (db != null)
            {
                return await db.Employees.ToListAsync();
            }

            return null;
        }

    }
}


    //    public IQueryable<Employee> GetEmployees()
    //    {
    //        return this.db.Employees;
    //    }
    //}

