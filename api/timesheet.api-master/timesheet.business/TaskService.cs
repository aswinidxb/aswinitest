﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    class TaskService : ITaskService
    {
        public TimesheetDb db { get; }
        public TaskService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public async Task<List<Tasks>> GeTTaskList()
        {
            if (db != null)
            {
                return await db.Tasks.ToListAsync();
            }

            return null;
        }

       

    }
}

