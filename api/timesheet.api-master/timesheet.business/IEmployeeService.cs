﻿using System.Collections.Generic;
using System.Threading.Tasks;
using timesheet.model;

namespace timesheet.business
{
    public interface IEmployeeService
    {
        Task<List<Employee>> GetEmployees();
    }
}