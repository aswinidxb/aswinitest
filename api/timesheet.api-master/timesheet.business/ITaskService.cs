﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using timesheet.model;

namespace timesheet.business
{
    public interface ITaskService
    {
        Task<List<Tasks>> GeTTaskList();
    }
}
