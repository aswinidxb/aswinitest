﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        ITaskService taskService;
        public TaskController(ITaskService _taskService)
        {
            taskService = _taskService;
        }

        [HttpGet]
        [Route("getallTask")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var items = await taskService.GeTTaskList();
                if (items == null)
                {
                    return NotFound();
                }
                return Ok(items);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}